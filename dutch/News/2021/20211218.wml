#use wml::debian::translation-check translation="43ee4d624248d2eac95a5997ba86398be35914ec"
<define-tag pagetitle>Debian 11 werd bijgewerkt: 11.2 is uitgebracht</define-tag>
<define-tag release_date>2021-12-18</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de tweede update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>). Deze
tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian
<release> is, maar slechts een update van enkele van de meegeleverde pakketten.
Het is niet nodig om oude media met <q><codename></q> weg te gooien. Na de
installatie kunnen pakketten worden opgewaardeerd naar de huidige versie,
afkomstig van een bijgewerkte Debian-spiegelserver.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
locaties.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst van spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Deze update van stable, de stabiele release, voegt een paar belangrijke correcties toe aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction authheaders "Nieuwe bovenstroomse release met bugreparaties">
<correction base-files "Update van /etc/debian_version voor de tussenrelease 11.2">
<correction bpftrace "Reparatie van array-indexering">
<correction brltty "Werking onder X herstellen bij gebruik van sysvinit">
<correction btrbk "Herstellen van een regressie in de update voor CVE-2021-38173">
<correction calibre "Syntaxisfout herstellen">
<correction chrony "Reparatie van het binden van een socket aan een netwerkapparaat met een naam die langer is dan 3 tekens wanneer de systeemoproepfilter is ingeschakeld">
<correction cmake "PostgreSQL 13 toevoegen aan de gekende versies">
<correction containerd "Nieuwe bovenstroomse stabiele release; dubbelzinnige ontleding van OCI-manifest [CVE-2021-41190] afhandelen; ondersteuning bieden voor <q>clone3</q> in het standaard seccomp-profiel">
<correction curl " -ffile-prefix-map verwijderen uit curl-config, om co-installeerbaarheid van libcurl4-gnutls-dev onder multiarch te herstellen">
<correction datatables.js "Reparatie voor het onvoldoende escapen van arrays die doorgegeven worden aan de HTML-functie escape entities [CVE-2021-23445]">
<correction debian-edu-config "pxe-addfirmware: TFTP-serverpad herstellen; verbeteren van ondersteuning voor het opzetten en onderhouden van LTSP-chroot">
<correction debian-edu-doc "Bijwerken van de handleiding voor Debian Edu Bullseye vanuit de wiki; bijwerken van de vertalingen">
<correction debian-installer "Herbouwen tegen proposed-updates; kernel-ABI updaten naar -10">
<correction debian-installer-netboot-images "Herbouwen tegen proposed-updates">
<correction distro-info-data "Bijwerken van ingesloten gegevens voor Ubuntu 14.04 en 16.04 ESM; toevoegen van Ubuntu 22.04 LTS">
<correction docker.io "Reparatie voor een mogelijke wijziging van de bestandssysteemrechten van de computer [CVE-2021-41089]; bestandsrechten vergrendelen in /var/lib/docker [CVE-2021-41091]; voorkomen dat identificatiegegevens naar het standaardregister worden verzonden [CVE-2021-41092]; ondersteuning toevoegen voor de syscall <q>clone3</q> in het standaard seccomp-beleid">
<correction edk2 "Kwetsbaarheid voor TOCTOU van Boot Guard aanpakken [CVE-2019-11098]">
<correction freeipmi "De pkgconfig-bestanden op de juiste locatie installeren">
<correction gdal "Reparatie van de ondersteuning voor BAG 2.0 Extract in de LVBAG-driver">
<correction gerbv "Probleem met schrijven buiten het bereik oplossen [CVE-2021-40391]">
<correction gmp "Probleem met gehele-getallen- en bufferoverloop oplossen [CVE-2021-43618]">
<correction golang-1.15 "Nieuwe bovenstroomse stabiele release; oplossing voor <q>net/http: panic due to racy read of persistConn after handler panic</q> [CVE-2021-36221]; oplossing voor <q>archive/zip: overflow in preallocation check can cause OOM panic</q> [CVE-2021-39293]; oplossen van een probleem met bufferoverloop [CVE-2021-38297], van een probleem met lezen buiten het bereik [CVE-2021-41771] en van denial of service problemen [CVE-2021-44716 CVE-2021-44717]">
<correction grass "Reparatie van het verwerken van GDAL-indelingen waarbij de beschrijving een dubbele punt bevat">
<correction horizon "Vertalingen opnieuw activeren">
<correction htmldoc "Problemen met bufferoverloop oplossen [CVE-2021-40985 CVE-2021-43579]">
<correction im-config "De voorkeur geven aan Fcitx5 boven Fcitx4">
<correction isync "Verschillende problemen met bufferoverloop oplossen [CVE-2021-3657]">
<correction jqueryui "Problemen met de uitvoering van niet-vertrouwde code verhelpen [CVE-2021-41182 CVE-2021-41183 CVE-2021-41184]">
<correction jwm "Crash repareren bij gebruik van het menu-item <q>Move</q>">
<correction keepalived "Te ruim DBus-beleid repareren [CVE-2021-44225]">
<correction keystone "Oplossen van informatielek waardoor kan worden vastgesteld of gebruikers bestaan [CVE-2021-38155]; enkele prestatieverbeteringen toepassen op de standaard keystone-uwsgi.ini">
<correction kodi "Bufferoverloop in PLS-afspeellijsten repareren [CVE-2021-42917]">
<correction libayatana-indicator "Pictogrammen schalen bij laden vanuit een bestand; regelmatige crashes in indicator-applets voorkomen">
<correction libdatetime-timezone-perl "Bijwerken van bijgevoegde gegevens">
<correction libencode-perl "Reparatie voor een geheugenlek in Encode.xs">
<correction libseccomp "Ondersteuning toevoegen voor syscalls tot Linux 5.15">
<correction linux "Nieuwe bovenstroomse release; ABI verhogen naar 10; RT: updaten naar 5.10.83-rt58">
<correction linux-signed-amd64 "Nieuwe bovenstroomse release; ABI verhogen naar 10; RT: updaten naar 5.10.83-rt58">
<correction linux-signed-arm64 "Nieuwe bovenstroomse release; ABI verhogen naar 10; RT: updaten naar 5.10.83-rt58">
<correction linux-signed-i386 "Nieuwe bovenstroomse release; ABI verhogen naar 10; RT: updaten naar 5.10.83-rt58">
<correction lldpd "Probleem met stapeloverloop oplossen [CVE-2021-43612]; de VLAN-tag niet instellen als de client deze niet ingesteld heeft">
<correction mrtg "Correctie voor fouten in de naam van variabelen">
<correction node-getobject "Probleem van prototypeverontreiniging oplossen [CVE-2020-28282]">
<correction node-json-schema "Probleem van prototypeverontreiniging oplossen [CVE-2021-3918]">
<correction open3d "Ervoor zorgen dat python3-open3d python3-numpy vereist">
<correction opendmarc "Reparatie van opendmarc-import; de maximaal ondersteunde lengte van tokens in ARC_Seal headers verhogen, oplossing voor crashes">
<correction plib "Oplossing voor een probleem met hele-getallenoverloop [CVE-2021-38714]">
<correction plocate "Probleem opgelost waarbij niet-ASCII karakters foutief werden ge-escaped">
<correction poco "Installatie van CMake-bestanden repareren">
<correction privoxy "Geheugenlekken herstellen [CVE-2021-44540 CVE-2021-44541 CVE-2021-44542]; probleem met cross-site scripting oplossen [CVE-2021-44543]">
<correction publicsuffix "Bijwerken van opgenomen gegevens">
<correction python-django "Nieuwe bovenstroomse beveiligingsrelease: mogelijke omzeiling van een bovenstroomse toegangscontrole gebaseerd op URL-paden repareren [CVE-2021-44420]">
<correction python-eventlet "Compatibiliteit herstellen met dnspython 2">
<correction python-virtualenv "Oplossing voor een crash bij het gebruik van --no-setuptools">
<correction ros-ros-comm "Een probleem van denial of service oplossen [CVE-2021-37146]">
<correction ruby-httpclient "Systeemcertificaatarchief gebruiken">
<correction rustc-mozilla "Nieuw bronpakket om het bouwen van nieuwere versies van firefox-esr en thunderbird te ondersteunen">
<correction supysonic "Een symbolische koppeling maken naar jquery in plaats van het direct te laden; geminimaliseerde bootstrap CSS-bestanden op een correcte wijze symbolisch koppelen">
<correction tzdata "Bijwerken van gegevens voor Fiji en Palestine">
<correction udisks2 "Aankoppelopties: altijd errors=remount-ro gebruiken voor ext-bestandssystemen [CVE-2021-3802]; het commando mkfs gebruiken om exfat-partities te formatteren; toevoegen van Recommends exfatprogs als voorkeursalternatief">
<correction ulfius "Gebruik van aangepaste toewijzingen repareren met ulfius_url_decode en ulfius_url_encode">
<correction vim "Repareren van stapeloverlopen [CVE-2021-3770 CVE-2021-3778], gebruik na een vrijgaveprobleem [CVE-2021-3796]; verwijderen van vim-gtk-alternativen tijdens de transitie vim-gtk -&gt; vim-gtk3, wat opwaarderingen vanaf buster vergemakkelijkt">
<correction wget "Reparatie voor downloads van meer dan 2GB op 32-bitssystemen">
</table>


<h2>Beveiligingsupdates</h2>


<p>Deze revisie voegt de volgende beveiligingsupdates toe aan de stabiele release. Het beveiligingsteam heeft voor elk van deze updates al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2021 4980 qemu>
<dsa 2021 4981 firefox-esr>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4984 flatpak>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4988 libreoffice>
<dsa 2021 4989 strongswan>
<dsa 2021 4992 php7.4>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4996 wpewebkit>
<dsa 2021 4998 ffmpeg>
<dsa 2021 5002 containerd>
<dsa 2021 5003 ldb>
<dsa 2021 5003 samba>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5007 postgresql-13>
<dsa 2021 5008 node-tar>
<dsa 2021 5009 tomcat9>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5016 nss>
<dsa 2021 5017 xen>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5022 apache-log4j2>
</table>



<h2>Het Debian-installatieprogramma</h2>
<p>Het installatieprogramma werd bijgewerkt om de reparaties die met deze tussenrelease in de stabiele distributie opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een vereniging van ontwikkelaars van vrije software
die vrijwillig tijd en moeite investeren in het produceren van het volledig
vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
distributie op &lt;debian-release@lists.debian.org&gt;.</p>


