<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6610">CVE-2014-6610</a>

      <p>Asterisk Open Source 11.x before 11.12.1 and 12.x before 12.5.1
      and Certified Asterisk 11.6 before 11.6-cert6, when using the
      res_fax_spandsp module, allows remote authenticated users to
      cause a denial of service (crash) via an out of call message,
      which is not properly handled in the ReceiveFax dialplan
      application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4046">CVE-2014-4046</a>

      <p>Asterisk Open Source 11.x before 11.10.1 and 12.x before 12.3.1
      and Certified Asterisk 11.6 before 11.6-cert3 allows remote
      authenticated Manager users to execute arbitrary shell commands
      via a MixMonitor action.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2286">CVE-2014-2286</a>

      <p>main/http.c in Asterisk Open Source 1.8.x before 1.8.26.1, 11.8.x
      before 11.8.1, and 12.1.x before 12.1.1, and Certified Asterisk
      1.8.x before 1.8.15-cert5 and 11.6 before 11.6-cert2, allows remote
      attackers to cause a denial of service (stack consumption) and
      possibly execute arbitrary code via an HTTP request with a large
      number of Cookie headers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-8412">CVE-2014-8412</a>

      <p>The (1) VoIP channel drivers, (2) DUNDi, and (3) Asterisk Manager
      Interface (AMI) in Asterisk Open Source 1.8.x before 1.8.32.1,
      11.x before 11.14.1, 12.x before 12.7.1, and 13.x before 13.0.1
      and Certified Asterisk 1.8.28 before 1.8.28-cert3 and 11.6 before
      11.6-cert8 allows remote attackers to bypass the ACL restrictions
      via a packet with a source IP that does not share the address family
      as the first ACL entry.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-8418">CVE-2014-8418</a>

      <p>The DB dialplan function in Asterisk Open Source 1.8.x before 1.8.32,
      11.x before 11.1.4.1, 12.x before 12.7.1, and 13.x before 13.0.1 and
      Certified Asterisk 1.8 before 1.8.28-cert8 and 11.6 before 11.6-cert8
      allows remote authenticated users to gain privileges via a call from
      an external protocol, as demonstrated by the AMI protocol.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-3008">CVE-2015-3008</a>

      <p>Asterisk Open Source 1.8 before 1.8.32.3, 11.x before 11.17.1, 12.x
      before 12.8.2, and 13.x before 13.3.2 and Certified Asterisk 1.8.28
      before 1.8.28-cert5, 11.6 before 11.6-cert11, and 13.1 before
      13.1-cert2, when registering a SIP TLS device, does not properly
      handle a null byte in a domain name in the subject's Common Name (CN)
      field of an X.509 certificate, which allows man-in-the-middle attackers
      to spoof arbitrary SSL servers via a crafted certificate issued by a
      legitimate Certification Authority.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these issues have been fixed in asterisk version 1:1.8.13.1~dfsg1-3+deb7u4</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-455.data"
# $Id: $
