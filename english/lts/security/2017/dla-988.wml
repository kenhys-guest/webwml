<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that RT::Authen::ExternalAuth, an external
authentication module for Request Tracker, is vulnerable to timing
side-channel attacks for user passwords. Only ExternalAuth in DBI
(database) mode is vulnerable.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.10-4+deb7u1.</p>

<p>We recommend that you upgrade your rt-authen-externalauth packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-988.data"
# $Id: $
