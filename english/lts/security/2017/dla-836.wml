<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Stevie Trujillo discovered a command injection vulnerability in munin,
a network-wide graphing framework. The CGI script for drawing graphs
allowed to pass arbitrary GET parameters to local shell command,
allowing command execution as the user that runs the webserver.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.0.6-4+deb7u3.</p>

<p>We recommend that you upgrade your munin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-836.data"
# $Id: $
