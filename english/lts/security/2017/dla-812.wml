<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in ikiwiki, a wiki compiler:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9646">CVE-2016-9646</a>

    <p>Commit metadata forgery</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10026">CVE-2016-10026</a>

    <p>Authorization bypass when reverting changes</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0356">CVE-2017-0356</a>

    <p>Authentication bypass via repeated parameters</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.20120629.2+deb7u2.</p>

<p>We recommend that you upgrade your ikiwiki packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-812.data"
# $Id: $
