<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In junit4 the test rule TemporaryFolder contains a local information disclosure vulnerability. On Unix like systems, the system's temporary directory is shared between all users on that system. Because of this, when files and directories are written into this directory they are, by default, readable by other users on that same system. This vulnerability does not allow other users to overwrite the contents of these directories or files. This is purely an information disclosure vulnerability. This vulnerability impacts you if the JUnit tests write sensitive information, like API keys or passwords, into the temporary folder, and the JUnit tests execute in an environment where the OS has other untrusted users.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.12-4+deb9u1.</p>

<p>We recommend that you upgrade your junit4 packages.</p>

<p>For the detailed security status of junit4 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/junit4">https://security-tracker.debian.org/tracker/junit4</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2426.data"
# $Id: $
