<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that clamav, the open source antivirus engine, is affected by
the following security vulnerabilities:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12625">CVE-2019-12625</a>

    <p>Denial of Service (DoS) vulnerability, resulting from excessively long scan
    times caused by non-recursive zip bombs. Among others, this issue was
    mitigated by introducing a scan time limit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12900">CVE-2019-12900</a>

    <p>Out-of-bounds write in ClamAV's NSIS bzip2 library when attempting
    decompression in cases where the number of selectors exceeded the max limit
    set by the library.</p>

<p>This update triggers a transition from libclamav7 to libclamav9. As a result,
several other packages will be recompiled against the fixed package after the
release of this update: dansguardian, havp, python-pyclamav, c-icap-modules.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.101.4+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1953.data"
# $Id: $
