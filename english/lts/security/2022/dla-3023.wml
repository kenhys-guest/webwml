<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in puma, a web server for
Ruby/Rack applications. These flaws may lead to information leakage due to not
always closing response bodies, allowing untrusted input in a response header
(HTTP Response Splitting) and thus potentially facilitating several other
attacks like cross-site scripting. A poorly-behaved client could also use
keepalive requests to monopolize Puma's reactor and create a denial of service
attack.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
3.6.0-1+deb9u2.</p>

<p>We recommend that you upgrade your puma packages.</p>

<p>For the detailed security status of puma please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/puma">https://security-tracker.debian.org/tracker/puma</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3023.data"
# $Id: $
