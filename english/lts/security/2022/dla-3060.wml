<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in blender, a very fast and versatile 3D
modeller/renderer.</p>

<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0546">CVE-2022-0546</a>

      <p>An out-of-bounds heap access due to missing checks in the image
      loader could result in denial of service, memory corruption or
      potentially code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0545">CVE-2022-0545</a>

      <p>An integer overflow while processing 2d images might result in a
      write-what-where vulnerability or an out-of-bounds read vulnerability
      which could leak sensitive information or achieve code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0544">CVE-2022-0544</a>

      <p>Crafted DDS image files could create an integer underflow in the
      DDS loader which leads to an out-of-bounds read and might leak
      sensitive information.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
2.79.b+dfsg0-1~deb9u2.</p>

<p>We recommend that you upgrade your blender packages.</p>

<p>For the detailed security status of blender please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/blender">https://security-tracker.debian.org/tracker/blender</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3060.data"
# $Id: $
