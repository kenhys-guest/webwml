<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>There were a couple of secuity issues found in sysstat, system
performance tools for Linux, which are as follows:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16167">CVE-2019-16167</a>

    <p>sysstat before 12.1.6 has memory corruption due to an Integer
    Overflow in remap_struct() in sa_common.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19725">CVE-2019-19725</a>

    <p>sysstat through 12.2.0 has a double free in check_file_actlst
    in sa_common.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39377">CVE-2022-39377</a>

    <p>On 32 bit systems, allocate_structures contains a size_t overflow
    in sa_common.c. The allocate_structures function insufficiently
    checks bounds before arithmetic multiplication, allowing for an
    overflow in the size allocated for the buffer representing system
    activities. This issue may lead to Remote Code Execution (RCE).</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
12.0.3-2+deb10u1.</p>

<p>We recommend that you upgrade your sysstat packages.</p>

<p>For the detailed security status of sysstat please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sysstat">https://security-tracker.debian.org/tracker/sysstat</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3188.data"
# $Id: $
