<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In PostGIS, which adds support for geographic objects to the PostgreSQL
database, denial of service via crafted ST_AsX3D function input was
fixed.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.3.1+dfsg-2+deb9u1.</p>

<p>We recommend that you upgrade your postgis packages.</p>

<p>For the detailed security status of postgis please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postgis">https://security-tracker.debian.org/tracker/postgis</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2857.data"
# $Id: $
