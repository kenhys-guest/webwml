<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in mbed TLS, a lightweight crypto
and SSL/TLS library, which could result in denial of service, information
disclosure or side-channel attacks.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.4.2-1+deb9u4.</p>

<p>We recommend that you upgrade your mbedtls packages.</p>

<p>For the detailed security status of mbedtls please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mbedtls">https://security-tracker.debian.org/tracker/mbedtls</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2826.data"
# $Id: $
