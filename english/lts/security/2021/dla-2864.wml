<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In ruby-haml, which is an elegant, structured XHTML/XML templating
engine, when using user input to perform tasks on the server,
characters like < > " ' must be escaped properly. In this case,
the ' character was missed. An attacker can manipulate the input to
introduce additional attributes, potentially executing code.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.0.7-1+deb9u1.</p>

<p>We recommend that you upgrade your ruby-haml packages.</p>

<p>For the detailed security status of ruby-haml please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-haml">https://security-tracker.debian.org/tracker/ruby-haml</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2864.data"
# $Id: $
