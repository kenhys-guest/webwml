<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in OpenCV, the Open
Computer Vision Library. Buffer overflows, NULL pointer dereferences and
out-of-bounds write errors may lead to a denial-of-service or other
unspecified impact.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.4.9.1+dfsg1-2+deb9u1.</p>

<p>We recommend that you upgrade your opencv packages.</p>

<p>For the detailed security status of opencv please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/opencv">https://security-tracker.debian.org/tracker/opencv</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2799.data"
# $Id: $
