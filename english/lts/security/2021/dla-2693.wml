<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The XML parsers used by XMLBeans did not set the properties needed to protect
the user from malicious XML input. Vulnerabilities include the possibility for
XML Entity Expansion attacks which could lead to a denial-of-service. This
update implements sensible defaults for the XML parsers to prevent these kind
of attacks.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.6.0+dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your xmlbeans packages.</p>

<p>For the detailed security status of xmlbeans please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/xmlbeans">https://security-tracker.debian.org/tracker/xmlbeans</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2693.data"
# $Id: $
