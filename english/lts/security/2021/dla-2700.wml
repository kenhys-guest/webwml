<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A buffer overflow was discovered in HTMLDOC, a HTML processor that
generates indexed HTML, PS, and PDF, which could potentially result
in the execution of arbitrary code. In addition a number of crashes
were addressed.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.8.27-8+deb9u1.</p>

<p>We recommend that you upgrade your htmldoc packages.</p>

<p>For the detailed security status of htmldoc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/htmldoc">https://security-tracker.debian.org/tracker/htmldoc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2700.data"
# $Id: $
