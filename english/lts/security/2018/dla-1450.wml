<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1304">CVE-2018-1304</a>

    <p>The URL pattern of "" (the empty string) which exactly maps to the
    context root was not correctly handled in Apache Tomcat when used as
    part of a security constraint definition. This caused the constraint
    to be ignored. It was, therefore, possible for unauthorized users to
    gain access to web application resources that should have been
    protected. Only security constraints with a URL pattern of the empty
    string were affected.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1305">CVE-2018-1305</a>

    <p>Security constraints defined by annotations of Servlets in Apache
    Tomcat were only applied once a Servlet had been loaded. Because
    security constraints defined in this way apply to the URL pattern
    and any URLs below that point, it was possible - depending on the
    order Servlets were loaded - for some security constraints not to be
    applied. This could have exposed resources to users who were not
    authorized to access them.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
8.0.14-1+deb8u12.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1450.data"
# $Id: $
