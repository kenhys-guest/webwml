<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An unsafe object deserialization vulnerability was found in jruby, a
100% pure-Java implementation of Ruby. An attacker can use this flaw
to run arbitrary code when gem owner is run on a specially crafted
YAML file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.5.6-5+deb7u2.</p>

<p>We recommend that you upgrade your jruby packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1352.data"
# $Id: $
