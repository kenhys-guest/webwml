<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A couple of vulnerabilities were reported against ruby-git, a Ruby
interface to the Git revision control system, that could lead to a
command injection and execution of an arbitrary ruby code by having
a user to load a repository containing a specially crafted filename
to the product.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1.2.8-1+deb10u1.</p>

<p>We recommend that you upgrade your ruby-git packages.</p>

<p>For the detailed security status of ruby-git please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-git">https://security-tracker.debian.org/tracker/ruby-git</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3303.data"
# $Id: $
