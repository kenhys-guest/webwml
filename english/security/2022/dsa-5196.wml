<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been found in libpgjava, the official
PostgreSQL JDBC Driver.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13692">CVE-2020-13692</a>

    <p>An XML External Entity (XXE) weakness was found in PostgreSQL JDBC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21724">CVE-2022-21724</a>

    <p>The JDBC driver did not verify if certain classes implemented the expected
    interface before instantiating the class. This can lead to code execution
    loaded via arbitrary classes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26520">CVE-2022-26520</a>

    <p>An attacker (who controls the jdbc URL or properties) can call
    java.util.logging.FileHandler to write to arbitrary files through the
    loggerFile and loggerLevel connection properties.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 42.2.5-2+deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 42.2.15-1+deb11u1.</p>

<p>We recommend that you upgrade your libpgjava packages.</p>

<p>For the detailed security status of libpgjava please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libpgjava">\
https://security-tracker.debian.org/tracker/libpgjava</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5196.data"
# $Id: $
