#use wml::debian::template title="Programme de Jonathan Carter" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="2a518350959c639a6ef36ac0fee462845276108d" maintainer="Jean-Pierre Giraud"

<DIV>
<TABLE>
<TR><TD>
<BR>
<H1><BIG><B>Jonathan Carter</B></BIG><BR>
    <SMALL>Programme de DPL</SMALL><BR>
    <SMALL>14 mars 2022</SMALL>
</H1>
<H3>
<A HREF="mailto:jcc@debian.org"><TT>jcc@debian.org</TT></A><BR>
<A HREF="https://jonathancarter.org"><TT>https://jonathancarter.org</TT></A><BR>
<A HREF="https://wiki.debian.org/highvoltage"><TT>https://wiki.debian.org/highvoltage</TT></A>
</H3>
</TD></TR>
</TABLE>


<H2>1. Informations personnelles</H2>

<P>Bonjour, mon nom est Jonathan Carter, connu aussi comme <EM>highvoltage</EM>,
avec comme pseudonyme dans Debian <EM>jcc</EM>, et je suis candidat à un
troisième mandat de DPL.</P>

<P>Je suis un développeur Debian de 40 ans ayant plusieurs domaines d’intérêt
dans le projet Debian. Comme beaucoup d'autres individus et organisations qui
contribuent à Debian, je participe au projet parce que j'aime utiliser Debian
et que j'aimerais que Debian continue à vivre et à se développer.
Debian reste seule à fournir une distribution véritablement libre, en
fournissant également des versions stables et des mises à jour de sécurité en
même temps qu'une très large sélection de paquets logiciels.
Debian est un projet logiciel extrêmement important et son contrat social
ajoute au projet un caractère unique en mettant en avant les priorités de nos
utilisateurs de façon explicite. Nous ne dépendons pas d'actionnaires ou du
résultat financier du trimestre, mais nous travaillons plutôt tous ensemble
pour essayer de trouver les meilleures solutions pour nos utilisateurs. Bien
que cela ne soit pas toujours facile ou simple, je trouve cela très gratifiant
et que faire cela vaut la peine d'être fait.
</P>


<H2>2. Pourquoi suis-je candidat au poste de DPL (à nouveau)</H2>

<P>
Quand je briguais mon premier mandat de DPL, j'étais très anxieux sur comment
cela pouvait se terminer. Heureusement, deux ans plus tard, j'ai accompli deux
mandats dont je suis plutôt fier.
</p>

<P>
Alors, pourquoi encore un mandat ? L'an dernier, j'ai découvert que c'était
beaucoup plus difficile de faire avancer les choses pendant une année de
publication que pendant une année sans. Durant le gel, nous nous sommes
entièrement focalisés sur les derniers problèmes pour faire sortir la
version stable, et ce n'est pas le meilleur moment pour des résolutions
générales ou pour des discussions nécessitant une forte implication sur des
évolutions du projet. Peu de temps après la publication, des discussions ont
débuté pour améliorer notre processus de vote, suivies d'une autre résolution
générale pour procéder à davantage de modification sur le vote. Donc, pour mon
prochain mandat, j'aimerai faire aboutir (ou au moins faire des progrès
significatifs) sur des sujets qui n'ont pas avancé durant le dernier mandat.
J'aimerais apporter mon aide à certains développeurs Debian avec lesquels j'ai
parlé des modifications de microprogrammes dans Debian. Nous avons beaucoup de
positions différentes sur les microprogrammes dans le projet, mais je crois
que nous avons beaucoup de choses en commun sur lesquels nous pouvons
construire tout en utilisant notre influence pour orienter positivement
les constructeurs.
</P>


<H2>3. Programme</H2>


<H3> 3.1. Formaliser Debian et certaines de nos relations et de nos procédures </H3>

<P><B>Envisager un enregistrement officiel de l'organisation Debian.</B> J'ai
l'intention d'engager des discussions pour l'enregistrement de Debian en tant
qu'organisation officielle. En 2020, la campagne de DPL de Brian Gupta a tourné
largement autour de la création d'une fondation pour le projet Debian. Je suis
d'accord avec beaucoup des raisons pour lesquelles il souhaitait suivre cette
piste, et je souhaite proposer une version moins lourde de ce projet. C'est
probablement le bon moment pour clarifier le fait que ma campagne de DPL n'est
ni un référendum sur ce sujet, ni une promesse de campagne. Je crois que c'est
quelque chose dont nous devons nous occuper ensemble en tant que projet et
prendre une décision basée sur ses avantages. L'absence de dépôt de statuts
s'est accompagnée de nombreux problèmes, y compris des difficultés pour établir
des conventions avec des entités extérieures et en créant des problèmes en
matière de responsabilité légale personnelle au sein du projet. Je pense qu'une
approche légère qui pourrait corriger les problèmes existants tout en
préservant nos relations avec nos organismes habilités (« Trusted
Organizations ») qui font un travail important pour nous, est la meilleure voie
à suivre.</P>

<P><B>Organismes habilités.</B> Actuellement, nous dépendons trop d'accords
verbaux divers (et parfois même contradictoires) avec nos organismes habilités.
Une fois que Debian sera enregistrée, et nous sommes en mesure de le faire,
nous mettrons en œuvre quelques accords minimaux avec nos organismes habilités
pour codifier nos relations et responsabilités. Même si nous enregistrons
Debian comme un organisme propre, les organismes habilités continueront à jouer
un rôle vital dans la mesure où les services qu'ils rendent sont précieux et
qu'il serait fastidieux de les dupliquer sans une bonne raison.</P>

<P><B>Améliorer notre comptabilité.</B> L'équipe de trésorerie ne dispose pas
des moyens adéquats pour réaliser les tâches qui lui ont été attribuées dans sa
délégation. Suivre les actifs de Debian est un travail difficile, fastidieux et
non automatisé. En tant que projet, nous ne savons pas à un moment donné de
combien de fonds nous disposons, et à certains moments, travailler sur des
actifs comme les marques déposées a été une source de frustration. Une fois que
nous aurons la possibilité de conclure des accords formels avec les organismes
habilités, nous nous pourrons définir un certain nombres d'exigences et de
convention sur comment nous souhaitons partager les responsabilités et les
comptes avec les organismes habilités.</P>


<H3> 3.2. Technique </H3>

<P><B>Microprogrammes.</B> Les méthodes pour charger et distribuer les
microprogrammes ont changé de façon significatives ces dernières décennies.
Avoir des microprogrammes et des microcodes non mis à jour peut exposer à des
risques de sécurité importants, et beaucoup de nouveaux périphériques ne
stockent pas de copie intégrée permanente du microprogramme, nécessitant de
le charger à partir d'un disque. Cela a des conséquences importantes pour
Debian. Notre support d'installation libre par défaut n'est pas fourni avec
d'importantes mises à jour de microcode et avec notre média autonome, nous
rencontrons des problèmes avec les microprogrammes et les pilotes non libres,
faisant qu'un grand nombre de machines sont inutilisables avec ces supports. Je
ne défends pas l'idée d'inclure simplement des parties non libres sur tous nos
supports, mais je crois que nous pouvons faire des améliorations et que des
mesures peuvent être prises sans compromettre nos valeurs de base. J'aimerais
aussi me rapprocher à la fois de la FSF, l'OSI et la LF pour voir s'il y a une
possibilité pour que nous travaillons ensemble sur le problème des
microgrammes. Nous avons aussi pas mal de fonds disponibles et nous pourrions
mettre à disposition quelques fonds pour le développement de microprogrammes
libres dans les cas où cela semble possible.


<H2> 4. En conclusion </H2>

<P>Merci d'avoir pris le temps de lire mon programme. Outre le programme dont
j'ai tracé les grandes lignes plus haut, je continuerai à œuvrer à faire de
Debian un projet encore plus stable et accueillant. J'espère que les problèmes
auxquels nous faisons face dans le monde se résoudront et qu'ainsi nous pourront
à nouveau passer plus de temps ensemble en personne.</P>

<!--=
<H2> A. Réfutations </H2>

<H3> A.1. Felix Lechner </H3>

<H3> A.2. Hideki Yamane </H3>

<P>
</P>
-->

<H2> A. Journal des modifications </H2>

<P> Les versions de ce programme sont gérées dans un <a href="https://salsa.debian.org/jcc/dpl-platform">dépôt git.</a> </P>

<UL>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/4.0.0">4.0.0</a>: Nouveau programme pour les élections 2022 du DPL.</LI>
</UL>

<BR>

</DIV>
