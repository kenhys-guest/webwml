#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>L'implémentation des fonctions de printf() de libcurl déclenche un
dépassement de tampon lors de la production d'une grande sortie à virgule
flottante. Le bogue survient quand la conversion génère plus de 255 octets.</p>

<p>Ce défaut se produit parce que la conversion de nombres à virgule
flottante utilise les fonctions du système sans effectuer les vérifications
correctes de limites.</p>

<p>S’il existe une application qui accepte une chaîne de formatage de
l’extérieur sans filtrage nécessaire d’entrée, cela pourrait permettre des
attaques distantes.</p>

<p>Ce défaut n'existe pas dans l'outil en ligne de commande.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 7.26.0-1+wheezy18.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-767.data"
# $Id: $
