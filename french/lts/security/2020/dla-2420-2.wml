#use wml::debian::translation-check translation="dbb713a0eb271c910a768bd698aee03c156c8de2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour corrige une régression dans quelques environnements de
machine virtuelle Xen. Pour rappel, voici le texte de l’annonce originale.</p>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à l'exécution de code arbitraire, une élévation des
privilèges, un déni de service ou une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9445">CVE-2019-9445</a>

<p>Une lecture hors limites potentielle a été découverte dans l’implémentation
de F2FS. Un utilisateur, autorisé à monter et accéder à des systèmes de fichiers
arbitraires, pourrait éventuellement utiliser cela pour provoquer un déni de
service (plantage) ou pour lire des informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19073">CVE-2019-19073</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19074">CVE-2019-19074</a>

<p>Navid Emamdoost a découvert une fuite de mémoire potentielle dans les pilotes
ath9k et ath9k_htc. L’impact de sécurité est incertain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19448">CVE-2019-19448</a>

<p><q>Team bobfuzzer</q> a signalé un bogue dans Btrfs qui pourrait conduire
à une utilisation de mémoire après libération, et pourrait être déclenchée par des
images de système de fichiers contrefaites. Un utilisateur, autorisé à monter et
accéder à des systèmes de fichiers arbitraires, pourrait éventuellement utiliser
cela pour provoquer un déni de service (plantage) ou pour lire des informations
sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12351">CVE-2020-12351</a>

<p>Andy Nguyen a découvert un défaut dans l’implémentation du Bluetooth dans la
manière dont des paquets L2CAP avec CID A2MP sont gérés. Un attaquant distant
peu éloigné connaissant l’adresse du périphérique Bluetooth de la victime peut
envoyer un paquet l2cap malveillant et causer un déni de service ou,
éventuellement, une exécution de code arbitraire avec les privilèges du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12352">CVE-2020-12352</a>

<p>Andy Nguyen a découvert un défaut dans l’implémentation du Bluetooth. La
mémoire de pile n’est pas correctement initialisée lors du traitement de
certains paquets AMP. Un attaquant distant peu éloigné connaissant l’adresse du
périphérique Bluetooth de la victime peut récupérer des informations de la pile
du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12655">CVE-2020-12655</a>

<p>Zheng Bin a signalé que des volumes XFS contrefaits pourraient déclencher un
plantage du système. Un attaquant capable de monter un tel volume pourrait
utiliser cela pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12771">CVE-2020-12771</a>

<p>Zhiqiang Liu a signalé un bogue dans le pilote de bloc bcache qui pourrait
conduire à un plantage du système. L’impact de sécurité est incertain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12888">CVE-2020-12888</a>

<p>Il a été découvert que le pilote PCIe Virtual Function (vfio-pci)
permettait aux utilisateurs de désactiver un espace mémoire de périphérique
alors qu’il était encore mappé dans un processus. Sur certaines plateformes
matérielles, des utilisateurs locaux ou des machines virtuelles invitées pouvant
accéder à des fonctions PCIe virtuelles, pourraient utiliser cela pour provoquer
un déni de service (erreur de matériel et plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14305">CVE-2020-14305</a>

<p>Vasily Averin de Virtuozzo a découvert un dépassement de tampon de tas
potentiel dans le module nf_contrack_h323 de netfilter. Quand ce module est
utilisé pour le suivi de connexions pour TCP/IPv6, un attaquant distant pourrait
utiliser cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou, éventuellement, pour une exécution de code à distance avec les
privilèges du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14314">CVE-2020-14314</a>

<p>Un bogue a été découvert dans le système de fichiers ext4 qui pourrait
conduire à une lecture hors limites. Un utilisateur local autorisé à monter et
à accéder à des systèmes de fichiers arbitraires pourrait utiliser cela pour
provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14331">CVE-2020-14331</a>

<p>Un bogue a été découvert dans la fonction soft-scrollback du pilote de la
console VGA qui pourrait conduire à un dépassement de tampon de tas. Dans un
système avec un noyau personnalisé ayant CONFIG_VGACON_SOFT_SCROLLBACK
activé, un utilisateur local ayant accès à la console pourrait utiliser cela
pour provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14356">CVE-2020-14356</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2020-25220">CVE-2020-25220</a>

<p>Un bogue a été découvert dans le traitement du sous-système de cgroup des
références de socket à des cgroup. Dans certaines configurations de cgroup, cela
pourrait conduire à une utilisation de mémoire après libération. Un utilisateur
local pourrait utiliser cela pour provoquer un déni de service (plantage ou
corruption de mémoire) ou, éventuellement, pour une élévation des privilèges.</p>

<p>Le correctif original pour ce bogue introduisait un nouveau problème de
sécurité, qui est aussi corrigé dans cette mise à jour.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14386">CVE-2020-14386</a>

<p>Or Cohen a découvert un bogue dans l’implémentation de socket de paquet
(AF_PACKET) qui pourrait conduire à un dépassement de tampon de tas. Un
utilisateur local avec la capacité CAP_NET_RAW (dans n’importe quel espace de
noms utilisateur) pourrait utiliser cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou, éventuellement, pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14390">CVE-2020-14390</a>

<p>Minh Yuan a découvert un bogue dans la fonction <q>scrollback</q> du pilote
de la console de tampon de trame qui pourrait conduire à un dépassement de
tampon de tas. Dans un système utilisant les consoles de tampon de trame, un
utilisateur local avec accès à la console pourrait utiliser cela pour provoquer
un déni de service (plantage ou corruption de mémoire) ou, éventuellement, pour
une élévation des privilèges.</p>

<p>La fonction <q>scrollback</q> a été désactivée pour le moment et aucun autre
correctif n’est disponible pour ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15393">CVE-2020-15393</a>

<p>Kyungtae Kim a signalé une fuite de mémoire dans le pilote usbtest. L’impact
de sécurité est incertain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16166">CVE-2020-16166</a>

<p>Amit Klein a signalé que le générateur de nombres aléatoires utilisé par la
pile réseau pourrait n’être pas <q>regrainé</q> pendant de longues périodes,
rendant, par exemple, les allocations de numéro de port plus prévisibles. Cela
facilitait pour des attaquants distants la conduite d’attaques basées sur le
réseau telles que l’empoisonnement de cache DNS ou le suivi de périphérique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24490">CVE-2020-24490</a>

<p>Andy Nguyen a découvert un défaut dans l’implémentation du Bluetooth qui
peut conduire à un dépassement de tampon de tas. Dans les systèmes avec une
interface matérielle Bluetooth 5, un attaquant distant peu éloigné peut utiliser
cela pour provoquer un déni de service (plantage ou corruption de mémoire)
ou, éventuellement, pour une exécution de code à distance avec les privilèges
du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25211">CVE-2020-25211</a>

<p>Un défaut a été découvert dans le sous-système netfilter. Un attaquant local
capable d’injecter une configuration de conntrack de Netlink peut provoquer un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25212">CVE-2020-25212</a>

<p>Un bogue a été découvert dans l’implémentation de client NFSv4 qui pourrait
conduire à un dépassement de tampon de tas. Un serveur NFS malveillant pourrait
utiliser cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou, éventuellement, pour exécuter du code arbitraire sur le client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25284">CVE-2020-25284</a>

<p>Il a été découvert que le pilote de périphérique bloc Rados (rbd) permettait
aux tâches exécutées avec l’UID 0 d’ajouter ou de retirer des périphériques rbd,
même si elles avaient des capacités retirées. Dans un système avec le pilote
rbd chargé, cela pouvait permettre une élévation des privilèges d’un conteneur
avec une tâche exécutée en tant que superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25285">CVE-2020-25285</a>

<p>Une situation de compétition a été découverte dans les gestionnaires sysctl
du système de fichiers hugetlb, qui pourrait conduire à une corruption de
pile. Un utilisateur local pouvant écrire dans les sysctl de superpages pourrait
utiliser cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou, éventuellement, pour une élévation des privilèges. Par défaut, seul
le superutilisateur peut faire cela.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25641">CVE-2020-25641</a>

<p>L’outil syzbot a trouvé un bogue dans la couche de blocs qui pourrait
conduire à une boucle infinie. Un utilisateur local avec accès à un
périphérique bloc brut pourrait utiliser cela pour provoquer un déni de service
(utilisation non limitée de CPU et plantage possible du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25643">CVE-2020-25643</a>

<p>ChenNan de Chaitin Security Research Lab a découvert un défaut dans le
module hdlc_ppp. Une validation incorrecte d’entrée dans la fonction
ppp_cp_parse_cr() pourrait conduire à une corruption de mémoire et une
divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26088">CVE-2020-26088</a>

<p>Il a été découvert que l’implémentation de socket NFC (Near Field
Communication) permettait à n’importe quel utilisateur de créer des sockets
bruts. Dans un système avec une interface NFC, cela permettait à des
utilisateurs locaux de s’affranchir de la politique de sécurité réseau locale.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.9.240-1. De plus, cette mise à jour ajoute beaucoup d’autres
corrections de bogues depuis les mises à jour, version 4.9.229-4.9.240 comprise.</p>
<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2420-2.data"
# $Id: $
