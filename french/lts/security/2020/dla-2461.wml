#use wml::debian::translation-check translation="d4b035099c6fd45fca0592ae606f4b629676f5f5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Zabbix, une solution de
supervision de réseau. Un attaquant peut exécuter à distance du code sur le
serveur zabbix et rediriger vers des liens externes à travers le frontal web
de Zabbix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10742">CVE-2016-10742</a>

<p>Zabbix permet l’ouverture d’un réacheminement à l’aide du paramètre de la
requête.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11800">CVE-2020-11800</a>

<p>Zabbix permet à des attaquants distants d’exécuter du code arbitraire.</p></li>

</ul>

<p>Cette mise à jour inclut plusieurs autres corrections de bogue et des
améliorations. Pour plus d’informations, veuillez vous référer au journal de
modifications de l’amont.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:3.0.31+dfsg-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zabbix.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zabbix, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zabbix">https://security-tracker.debian.org/tracker/zabbix</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2461.data"
# $Id: $
