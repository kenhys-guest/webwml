#use wml::debian::translation-check translation="fa7c3afe58dcc141abf94a973eacd63922508af9" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Log4j 1.2
d'Apache, une infrastructure de journalisation pour Java, quand il est
configuré pour utiliser JMSSink, JDBCAppender, JMSAppender ou Apache
Chainsaw. Elles pourraient être exploitées pour une exécution de code
distant.</p>

<p>Notez qu'un attaquant potentiel doit avoir accès en écriture à la
configuration de Log4j et que les fonctionnalités susmentionnées ne sont
pas activées par défaut. Pour atténuer complètement ce type de
vulnérabilité, les classes associées ont été supprimées du fichier jar
résultant.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.2.17-7+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache-log4j1.2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache-log4j1.2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/apache-log4j1.2">\
https://security-tracker.debian.org/tracker/apache-log4j1.2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2905.data"
# $Id: $
