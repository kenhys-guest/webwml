#use wml::debian::translation-check translation="dd1a499928293369d17fe7201d8457370101a0c7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle de déni de
service à distance dans node-trim-newlines, un module de Javascript pour
retirer les caractères de nouvelle ligne au début ou à la fin d’une chaine.
</p>

<p>Cette attaque de service (RedoS) à l’aide d’une expression rationnelle
exploitait le fait que la plupart des implémentations d’expressions rationnelles
peuvent faire face à des situations extrêmes faisant qu’elles fonctionnent avec
une lenteur croissant de manière exponentielle avec la taille de l’entrée. Un attaquant
pouvait faire qu’un programme utilisant node-trim-newlines (et par conséquent
l’expression rationnelle incriminée) entre dans une situation extrême et
ne réponde qu'après un long délai.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33623">CVE-2021-33623</a>

<p>Les paquets trim-newlines avant les versions 3.0.1 et 4.x avant 4.0.1 pour
Node.js avaient un problème concernant un déni de service à l’aide d’une
expression rationnelle (ReDoS) pour la méthode <q>.end()</q>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.0.0-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-trim-newlines.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3247.data"
# $Id: $
