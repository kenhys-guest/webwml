#use wml::debian::translation-check translation="97319e6620fa2185613b630e61b623c365b2cfe2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le serveur IMAP Cyrus était vulnérable à une attaque par déni de service
au moyen d'une entrée mal gérée pendant l'interaction avec la table de
hachage. En outre, il permettait une élévation de privilèges parce qu'une
requête HTTP peut être interprétée dans le contexte d'authentification
d'une requête précédente sans lien, arrivée sur la même connexion.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.5.10-3+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cyrus-imapd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cyrus-imapd,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/cyrus-imapd">\
https://security-tracker.debian.org/tracker/cyrus-imapd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3052.data"
# $Id: $
