#use wml::debian::translation-check translation="49c0fb4d13aefef76afb788706e5d13565f87c6b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un défaut a été découvert dans la manière dont l’analyseur de code source
d’exubertant-ctags gérait l’option <q>-o</q> de ligne de commande qui spécifie
le nom de fichier de marqueurs. Un nom de fichier de marqueurs contrefait
indiqué dans la ligne de commande ou dans le fichier de configuration pouvait
aboutir à une exécution de code arbitraire parce que externalSortTags() dans
sort.c appelle la fonction system(3) d’une manière non sécurisée.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4515">CVE-2022-4515</a>

<p>Un défaut a été découvert dans Exuberant Ctags dans la manière dont il gérait
l’option <q>-o</q>. Cette option précise le nom de fichier de marqueurs. Un nom
de fichier de marqueurs contrefait indiqué dans la ligne de commande ou dans le
fichier de configuration pouvait aboutir à une exécution de code arbitraire
parce que externalSortTags() dans sort.c appelle la fonction system(3) d’une
manière non sécurisée.</p>
</li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:5.9~svn20110310-12+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets exuberant-ctags.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3254.data"
# $Id: $
