#use wml::debian::translation-check translation="7b7af2d8dddff842fd215d6247e54d72d2a0472d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été identifiées dans le code VNC de ssvnc, un
client VNC avec possibilités de chiffrement.</p>

<p>Les vulnérabilités référencées ci-dessous sont des problèmes qui, à l’origine,
ont été signalés pour le paquet source libvncserver de Debian (qui fournit aussi
la bibliothèque partagée libvncclient). Le paquet source ssvnc dans Debian
incorpore une variante dépouillée et avec une correction personnalisée de
libvncclient, par conséquent quelques correctifs de sécurité pour libvncclient
nécessitent plus de portage.</p>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20020">CVE-2018-20020</a>

<p>LibVNC contenait une vulnérabilité d’écriture de tas hors limites dans une
structure dans le code du client VNC qui pourrait aboutir à l’exécution de code
à distance</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20021">CVE-2018-20021</a>

<p>LibVNC contenait une CWE-835 : vulnérabilité de boucle infinie dans le code
du client VNC. Cette vulnérabilité permet à un attaquant de consommer un montant
excessif de ressources telles que le CPU et la RAM</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20022">CVE-2018-20022</a>

<p>LibVNC contenait plusieurs vulnérabilités CWE-665 : initialisation incorrecte
dans le code du client VNC permettant à des attaquants de lire la mémoire de
pile et pouvant être mal utilisée pour une divulgation d'informations. Combinée
avec une autre vulnérabilité, cela pourrait être utilisé pour divulguer la
disposition de la mémoire de pile et contourner ASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20024">CVE-2018-20024</a>

<p>LibVNC contenait un déréférencement de pointeur NULL dans le code du client VNC
pouvant aboutir à un déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.0.29-2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ssvnc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2016.data"
# $Id: $
