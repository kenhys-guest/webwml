#use wml::debian::translation-check translation="16ffa8223d9a8de1ec4e5dabe84aede52bacede8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans la bibliothèque
de rendu partagée de PDF, poppler.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19058">CVE-2018-19058</a>

<p>Une interruption accessible dans Object.h peut aboutir à un déni de service
à cause d’un manque de vérification dans EmbFile::save2 de FileSpec.cc avant
de sauvegarder un fichier intégré.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20481">CVE-2018-20481</a>

<p>Poppler gère incorrectement des entrées XRef non allouées. Cela permet à des
attaquants distants de provoquer un déni de service (déréférencement de pointeur
NULL) à l'aide d'un document PDF contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20662">CVE-2018-20662</a>

<p>Poppler permet à des attaquants de provoquer un déni de service (plantage
d'application et erreur de segmentation) en contrefaisant un fichier PDF dans
lequel une structure de données xref est corrompue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7310">CVE-2019-7310</a>

<p>Une lecture hors limites de tampon basé sur le tas (due à une erreur
d’absence de signe d’entier dans la fonction XRef::getEntry dans XRef.cc)
permet à des attaquants distants de provoquer un déni de service (plantage
d'application) ou éventuellement d’avoir un impact non précisé à l'aide d'un
document PDF contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9200">CVE-2019-9200</a>

<p>Une écriture hors limites de tampon basé sur le tas existe dans
ImageStream::getLine() situé dans Stream.cc qui peut (par exemple) être
déclenchée par l’envoi d’un fichier PDF contrefait au binaire pdfimages. Il
permet à un attaquant de provoquer un déni de service (erreur de segmentation)
ou éventuellement d’avoir un impact non précisé.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 0.26.5-2+deb8u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets poppler.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1706.data"
# $Id: $
