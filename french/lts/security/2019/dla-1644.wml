#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été trouvées dans Policykit, un cadriciel pour gérer
la politique d'administration et les privilèges.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19788">CVE-2018-19788</a>

<p>Un traitement incorrect de très hauts UID dans Policykit pourrait résulter
dans un contournement d’authentification.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6133">CVE-2019-6133</a>

<p>Jann Horn de Google a trouvé que Policykit ne vérifie pas correctement si un
processus est déjà authentifié. Cela peut conduire à une réutilisation
d’authentification par un utilisateur différent.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 0.105-15~deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets policykit-1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1644.data"
# $Id: $
