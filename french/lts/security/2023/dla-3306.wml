#use wml::debian::translation-check translation="32428dd7b6164a7f5a6bee9ba799d5a4156caa6d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de déni de service
potentiel (DoS) dans Django, un cadriciel populaire de développement web.</p>

<p>Les valeurs analysées dans l’en-tête HTTP <code>Accept-Language</code> sont
mises en cache selon l’ordre de Django pour éviter des analyses répétitives.
Cela pouvait conduire à une attaque potentielle par déni de service à l’aide
d’une utilisation excessive de la mémoire si la valeur brute des en-têtes
<code>Accept-Language</code> étaient très grande.</p>

<p>Les en-têtes <code>Accept-Language</code> sont limitées à une taille maximale
expressément pour éviter ce problème.</p>


<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23969">CVE-2023-23969</a></li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:1.11.29-1+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3306.data"
# $Id: $
