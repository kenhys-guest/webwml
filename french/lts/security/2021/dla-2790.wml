#use wml::debian::translation-check translation="63fb49d20e3a3b58d171e1ecdd6f0aeab79d2c7d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Tenable a découvert que dans Babel, un ensemble d’outils pour
l’internationalisation d’applications Python, Babel.Locale permet à des
attaquants de charger des fichiers arbitraires locaux .dat (contenant des
objets Python sérialisés) à l’aide d’une traversée de répertoires, conduisant
à une exécution de code. Cette vulnérabilité était précédemment corrigée dans
le <a href="https://security-tracker.debian.org/tracker/CVE-2021-20095">CVE-2021-20095</a>
dans d’autres distributions et suites.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 2.3.4+dfsg.1-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-babel.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-babel,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-babel">\
https://security-tracker.debian.org/tracker/python-babel</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2790.data"
# $Id: $
