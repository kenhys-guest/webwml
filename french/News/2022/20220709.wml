#use wml::debian::translation-check translation="4c0e547d89fc6a0f2307d782bbce0a6ac7d74bc2" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 11.4</define-tag>
<define-tag release_date>2022-07-09</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la quatrième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction apache2 "Nouvelle version amont stable ; correction d'un problème de dissimulation de requête HTTP [CVE-2022-26377], problèmes de lecture hors limites [CVE-2022-28330 CVE-2022-28614 CVE-2022-28615], problèmes de déni de service [CVE-2022-29404 CVE-2022-30522], possible problème de lecture hors limites [CVE-2022-30556], possible problème de contournement d'authentification basée sur l'IP [CVE-2022-31813]">
<correction base-files "Mise à jour de /etc/debian_version pour cette version 11.4">
<correction bash "Correction d'une lecture hors limite d’un octet, provoquant la corruption de caractères multi-octets dans les substitutions de commande">
<correction clamav "Nouvelle version amont stable ; correction de sécurité [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction clementine "Ajout d'une dépendance manquante à libqt5sql5-sqlite">
<correction composer "Correction d'un problème d'injection de code [CVE-2022-24828] ; mise à jour du modèle de jeton de GitHub">
<correction cyrus-imapd "Présence d'un champ <q>uniqueid</q> dans toutes les boîtes aux lettres, corrigeant les mises à niveau vers la version 3.6">
<correction dbus-broker "Correction d'un problème de dépassement de tampon [CVE-2022-31212]">
<correction debian-edu-config "Acceptation d'un courriel du réseau local envoyé à root@&lt;mynetwork-names&gt; ; création des principaux d'hôte et de service de Kerberos s'ils n'existent pas encore ; assurance que libsss-sudo est installé sur les stations de travail itinérantes ; correction du nommage et de la visibilité des files d'impression ; prise en charge de krb5i sur les stations de travail sans disque ; squid : recherches DNSv4 préférées aux recherches DNSv6">
<correction debian-installer "Reconstruction avec proposed-updates ; passage de l'ABI du noyau Linux à la version 16 ; rétablissement de certaines cibles réseau armel (openrd)">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates ; passage de l'ABI du noyau Linux à la version 16 ; rétablissement de certaines cibles réseau armel (openrd)">
<correction distro-info-data "Ajout d'Ubuntu 22.10, Kinetic Kudu">
<correction docker.io "Ordonnancement de docker.service après containerd.service pour corriger l'arrêt de conteneurs ; passage explicite du chemin du socket containerd à dockerd pour assurer qu'il ne démarre pas containerd tout seul">
<correction dpkg "dpkg-deb : correction de conditions de fin de fichier inattendues lors de l'extraction de .deb ; libdpkg : pas de restriction des champs virtuels source:* pour les paquets installés ; Dpkg::Source::Package::V2 : correction systématique des permissions des archives amont (régression venue de DSA-5147-1]">
<correction freetype "Correction d'un problème de dépassement de tampon [CVE-2022-27404] ; correction de plantages [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Correction de problèmes de dépassement de tampon [CVE-2022-25308 CVE-2022-25309] ; correction de plantage [CVE-2022-25310]">
<correction ganeti "Nouvelle version amont ; correction de plusieurs problèmes de mise à niveau ; correction de migration en direct avec QEMU 4 et <q>security_model</q> de <q>user</q> ou de <q>pool</q>">
<correction geeqie "Correction du Ctrl clic dans une sélection de bloc">
<correction gnutls28 "Correction d'un mauvais calcul de SHA384 de SSSE3 ; correction d'un problème de déréférencement de pointeur NULL [CVE-2021-4209]">
<correction golang-github-russellhaering-goxmldsig "Correction d'un déréférencement de pointeur NULL provoqué par des signatures XML contrefaites [CVE-2020-7711]">
<correction grunt "Correction d'un problème de traversée de répertoires [CVE-2022-0436]">
<correction hdmi2usb-mode-switch "udev : ajout d'un suffixe aux nœuds de périphériques /dev/video pour les désambiguïser ; modification des règles udev à la priorité 70 pour qu'elles surviennent après 60-persistent-v4l.rules">
<correction hexchat "Ajout d'une dépendance manquante à python3-cffi-backend">
<correction htmldoc "Correction de boucle infinie [CVE-2022-24191], de problèmes de dépassement d'entier [CVE-2022-27114] et d'un problème de dépassement de tampon de tas [CVE-2022-28085]">
<correction knot-resolver "Correction d'un possible échec d'assertion dans des cas extrêmes de NSEC3 [CVE-2021-40083]">
<correction libapache2-mod-auth-openidc "Nouvelle version amont stable ; correction d'un problème de redirection ouverte [CVE-2021-39191] ; correction de plantage lors d'un rafraîchissement ou d'un redémarrage ">
<correction libintl-perl "Installation réelle de gettext_xs.pm">
<correction libsdl2 "Lecture hors limites évitée lors du chargement d'un fichier BMP mal formé [CVE-2021-33657] et durant la conversion de YUV à RGB">
<correction libtgowt "Nouvelle version amont stable pour prendre en charge la nouvelle version de telegram-desktop">
<correction linux "Nouvelle version amont stable ; passage de l'ABI à la version 16">
<correction linux-signed-amd64 "Nouvelle version amont stable ; passage de l'ABI à la version 16">
<correction linux-signed-arm64 "Nouvelle version amont stable ; passage de l'ABI à la version 16">
<correction linux-signed-i386 "Nouvelle version amont stable ; passage de l'ABI à la version 16">
<correction logrotate "Verrouillage ignoré si le fichier d'état est lisible par tous [CVE-2022-1348] ; analyse de configuration plus stricte afin d'éviter l'analyse de fichiers extérieurs tels que les vidages d'image mémoire">
<correction lxc "Mise à jour du serveur de clés GPG par défaut corrigeant la création de conteneurs utilisant le modèle <q>download</q>">
<correction minidlna "Validation des requêtes HTTP pour protéger contre les attaques de « DNS rebinding » [CVE-2022-26505]">
<correction mutt "Correction d'un problème de dépassement de tampon d'uudecode  [CVE-2022-1328]">
<correction nano "Correction de plusieurs bogues, y compris des corrections de plantages">
<correction needrestart "Détection des groupes de contrôle de sessions de services et d'utilisateur prenant en compte cgroup v2">
<correction network-manager "Nouvelle version amont stable">
<correction nginx "Correction de plantage quand libnginx-mod-http-lua est chargé et que init_worker_by_lua* est utilisé ; palliatif d'une attaque de confusion de contenu du protocole de la couche application dans le module Mail [CVE-2021-3618]">
<correction node-ejs "Correction d'un problème d'injection de modèle côté serveur [CVE-2022-29078]">
<correction node-eventsource "Retrait d'en-têtes sensibles lors d'une redirection vers une origine différente [CVE-2022-1650]">
<correction node-got "Redirection vers un socket Unix interdite [CVE-2022-33987]">
<correction node-mermaid "Correction de problèmes de scripts intersites [CVE-2021-23648 CVE-2021-43861]">
<correction node-minimist "Correction d'un problème de pollution de prototype [CVE-2021-44906]">
<correction node-moment "Correction d'un problème de traversée de répertoires [CVE-2022-24785]">
<correction node-node-forge "Correction de problèmes de vérification de signature [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-raw-body "Correction d'un problème potentiel de déni de service dans node-express, en utilisant node-iconv-lite plutôt que node-iconv">
<correction node-sqlite3 "Correction d'un problème de déni de service [CVE-2022-21227]">
<correction node-url-parse "Correction de problèmes de contournement d'authentification [CVE-2022-0686 CVE-2022-0691]">
<correction nvidia-cuda-toolkit "Utilisation des archives OpenJDK8 pour amd64 et ppc64el ; vérification de l'opérabilité du binaire de Java ; nsight-compute : déplacement du dossier « sections » vers un emplacement multi-architecture ; correction de l’ordre des versions de nvidia-openjdk-8-jre">
<correction nvidia-graphics-drivers "Nouvelle version amont ; passage à l'arbre 470 amont ; correction de problèmes de déni de service [CVE-2022-21813 CVE-2022-21814] ; correction d'un problème d'écriture hors limites [CVE-2022-28181], de problèmes de lecture hors limites [CVE-2022-28183], de problèmes de déni de service [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192]">
<correction nvidia-graphics-drivers-legacy-390xx "Nouvelle version amont ; correction de problèmes d'écriture hors limites [CVE-2022-28181 CVE-2022-28185]">
<correction nvidia-graphics-drivers-tesla-418 "Nouvelle version amont stable">
<correction nvidia-graphics-drivers-tesla-450 "Nouvelle version amont stable ; correction de problèmes d'écriture hors limites [CVE-2022-28181 CVE-2022-28185], d'un problème de déni de service [CVE-2022-28192]">
<correction nvidia-graphics-drivers-tesla-460 "Nouvelle version amont stable">
<correction nvidia-graphics-drivers-tesla-470 "Nouveau paquet, passage de la prise en charge de Tesla vers l'arbre 470 amont ; correction d'un problème d'écriture hors limites [CVE-2022-28181], d'un problème de lecture hors limites [CVE-2022-28183], de problèmes de déni de service [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192]">
<correction nvidia-persistenced "Nouvelle version amont ; passage à l'arbre 470 amont">
<correction nvidia-settings "Nouvelle version amont ; passage à l'arbre 470 amont">
<correction nvidia-settings-tesla-470 "Nouveau paquet, passage de la prise en charge de Tesla vers l'arbre 470 amont">
<correction nvidia-xconfig "Nouvelle version amont">
<correction openssh "seccomp : ajout de l'appel système pselect6_time64 sur les architectures 32 bits">
<correction orca "Correction de l'utilisation avec webkitgtk 2.36">
<correction php-guzzlehttp-psr7 "Correction d'une analyse d'en-tête incorrecte [CVE-2022-24775]">
<correction phpmyadmin "Correction de certaines requêtes SQL générant une erreur du serveur">
<correction postfix "Nouvelle version amont stable ; pas d'écrasement du transport par défaut défini par l'utilisateur dans postinst ; if-up.d : pas d'émission d'erreur si postfix ne peut pas encore envoyer de message">
<correction procmail "Correction d'un déréférencement de pointeur NULL">
<correction python-scrapy "Pas d'envoi de données d'authentification avec chaque requête [CVE-2021-41125] ; pas d'exposition de cookies inter-domaines lors des redirections [CVE-2022-0577]">
<correction ruby-net-ssh "Correction de l'authentification avec les systèmes utilisant OpenSSH 8.8">
<correction runc "Respect de defaultErrnoRet de seccomp ; pas de définition de capacités héritables [CVE-2022-29162]">
<correction samba "Correction d'échec de démarrage de winbind quand <q>allow trusted domains = no</q> est utilisé ; correction de l'authentification de Kerberos du MIT ; correction d'un problème de protection de partage au moyen d'une situation de compétition dans mkdir [CVE-2021-43566] ; correction d'un possible problème sérieux de corruption de données dû à l'empoisonnement de cache d'un client Windows ; correction de l'installation sur des systèmes non systemd">
<correction tcpdump "Mise à jour du profil d'AppArmor pour permettre l'accès aux fichiers *.cap et gestion de suffixes numériques dans les noms de fichiers ajoutés par -W">
<correction telegram-desktop "Nouvelle version amont stable, rétablissant la fonctionnalité">
<correction tigervnc "Correction du démarrage du bureau de GNOME lors de l'utilisation de tigervncserver@.service ; correction de l'affichage des couleurs lorsque vncviewer et le serveur X11 utilisent des boutismes différents">
<correction twisted "Correction d'un problème de divulgation d'informations avec des redirections inter-domaines [CVE-2022-21712], d'un problème de déni de service lors des négociations de connexion SSH [CVE-2022-21716], de problèmes de dissimulation de requête HTTP [CVE-2022-24801]">
<correction tzdata "Mise à jour des données du fuseau horaire de la Palestine ; mise à jour de la liste des secondes intercalaires">
<correction ublock-origin "Nouvelle version amont stable">
<correction unrar-nonfree "Correction d'un problème de traversée de répertoires [CVE-2022-30333]">
<correction usb.ids "Nouvelle version amont ; mise à jour des données incluses">
<correction wireless-regdb "Nouvelle version amont ; suppression du détournement ajouté par l'installateur assurant que les fichiers venant du paquet sont utilisés">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2021 4999 asterisk>
<dsa 2021 5026 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5107 php-twig>
<dsa 2022 5108 tiff>
<dsa 2022 5110 chromium>
<dsa 2022 5111 zlib>
<dsa 2022 5112 chromium>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5114 chromium>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5116 wpewebkit>
<dsa 2022 5117 xen>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5120 chromium>
<dsa 2022 5121 chromium>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5124 ffmpeg>
<dsa 2022 5125 chromium>
<dsa 2022 5127 linux-signed-amd64>
<dsa 2022 5127 linux-signed-arm64>
<dsa 2022 5127 linux-signed-i386>
<dsa 2022 5127 linux>
<dsa 2022 5128 openjdk-17>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5130 dpdk>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5133 qemu>
<dsa 2022 5134 chromium>
<dsa 2022 5136 postgresql-13>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5148 chromium>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5155 wpewebkit>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5161 linux-signed-amd64>
<dsa 2022 5161 linux-signed-arm64>
<dsa 2022 5161 linux-signed-i386>
<dsa 2022 5161 linux>
<dsa 2022 5162 containerd>
<dsa 2022 5163 chromium>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5166 slurm-wlm>
<dsa 2022 5167 firejail>
<dsa 2022 5168 chromium>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5174 gnupg2>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction elog "non maintenu ; problèmes de sécurité">
<correction python-hbmqtt "non maintenu et cassé">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
