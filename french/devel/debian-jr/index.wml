#use wml::debian::template title="Projet Debian Junior"
#use wml::debian::translation-check translation="1df7fb6f20f7d03e33cbc3ede19249bfa322abe2" maintainer="Jean-Pierre Giraud"

# Premier traducteur : Martin Quinson, 2002.
# Pierre Machard, 2002.
# Nicolas Bertolissio, 2004-2007.
# David Prévôt, 2007-2018.

<h2>Debian pour les enfants</h2>

<p> 
Ceci est un projet de « <a href="https://www.debian.org/blends/">Debian Pure Blend</a> »
(« Blend » pour faire court). Notre but est de faire de Debian un système
d'exploitation que les enfants de tous âges voudront utiliser. Notre premier
objectif est de produire quelque chose pour les enfants jusqu'à 8 ans.
Nous viserons ensuite la tranche d'âge 7 à 12 ans. En atteignant leurs
10 ans, ces enfants devraient pouvoir utiliser facilement une version de
Debian sans modification majeure.
</p>

<h3>Liste de diffusion</h3>

<p>
Debian a mis en place une liste de diffusion (en anglais) pour ce projet. Vous
pouvez vous y <a href="https://lists.debian.org/debian-jr/">abonner</a> ou en
consulter les archives.
</p>

<h3>Canal IRC</h3>

<p>
Nous disposons d'un canal de discussion en temps réel (en anglais lui
aussi), #debian-jr sur irc.debian.org et un serveur XMPP multiutilisateur
<a href="xmpp:debian-jr@conference.debian.org?join">debian-jr@conference.debian.org</a>.
</p>

<h3>Actualité</h3>

<p>
L'équipe dispose d'un <a href="https://debian-jr-team.pages.debian.net/blog/">Blog</a>
pour les actualités du projets et les nouvelles.</p>

<h3>Site de la communauté</h3>

<p>
Nous avons un <a href="https://wiki.debian.org/Teams/DebianJr">site de la communauté Debian Junior</a>,
où les développeurs et les utilisateurs sont invités à contribuer au projet
Debian Junior.
</p>

<h3>Comment aider ?</h3>

<p>
Faites-nous part de vos suggestions sur ce que Debian Junior pourrait faire, tout particulièrement si vous êtes prêt à participer à leur réalisation. 
</p>

<h3>Installation</h3>

<p>
Le paquet <a href="https://packages.debian.org/junior-doc">junior-doc</a>
contient le guide rapide (Quick Guide).
</p>

<h3>Paquets de Debian Junior</h3>

<p>
« Debian Pure Blends » fourni une <a
href="https://blends.debian.org/junior/tasks/">vue d'ensemble des paquet</a>
dignes d'intérêt pour le groupe d'utilisateurs concerné.
</p>

<h3>Objectifs de Debian Junior</h3>

<h4>Rendre Debian attirant pour les enfants</h4>

<p> 
Le premier objectif du projet Debian Junior est de faire de Debian un système
d'exploitation que nos enfants ont <i>envie</i> d'utiliser. Cela implique une
certaine sensibilité aux besoins des enfants tels qu'exprimés par les enfants
eux-mêmes. En tant que parents, développeurs, grands frères et soeurs,
administrateurs système, nous devons garder les oreilles et les yeux grand
ouverts pour découvrir ce qui rend les ordinateurs désirables pour les enfants.
Sans ceci, nous pourrions facilement nous détourner vers des objectifs comme
l'« ergonomie », la « simplicité », le « confort de maintenance » ou la
« robustesse », qui sont des objectifs très nobles pour Debian dans son
ensemble, mais trop larges pour répondre aux besoins spécifiques des enfants.
</p>

<h4>Applications</h4>

<p>
Naturellement, les enfants ont des besoins et des envies différents de ceux des
adultes. Leurs applications favorites peuvent être des jeux, des traitements de
texte, des éditeurs de texte, des programmes de dessin, etc.  L'objectif est
d'identifier les meilleures applications accessibles aux enfants au sein de
Debian, d'en ajouter en empaquetant celles qui ne font pas encore partie de
Debian, et de s'assurer que les applications sélectionnées sont raisonnablement
entretenues. Une des tâches importantes de l'implémentation consiste à fournir
des métapaquets pour simplifier aux administrateurs l'installation de groupes
d'applications pour enfants. Une autre est d'améliorer nos paquets pour en
simplifier l'usage pour les enfants. Cela peut être aussi simple que remplir
les trous dans la documentation, ou aussi complexe que de collaborer avec les
auteurs amont.
</p>

<h4>Gestion des comptes à l'épreuve des enfants</h4>

<p>
L'idée n'est pas nécessairement d'implémenter des mesures de sécurité solides,
cela dépasserait notre mandat. L'objectif est simplement de fournir aux
administrateurs de la documentation sur le réglage de leur système pour éviter
que les enfants ne « cassent » le système, n'accaparent toutes les ressources
du système ou ne fassent d'autres choses demandant une intervention constante
de l'administrateur système. Ce besoin est plus criant pour des enfants que
pour des adultes, car ils ont tendance à explorer le système, à le pousser dans
ses derniers retranchements, simplement pour voir ce qui se passe. La pagaille
résultante peut être à la fois amusante et frustrante. L'objectif ici est de
préserver votre santé mentale (ainsi que votre sens de l'humour) en tant
qu'administrateur système.
</p>

<h4>Apprendre à utiliser un ordinateur</h4>

<p>
Le souhait que la machine soit à l'épreuve des enfants doit être contrebalancé
par la nécessité d'<i>autoriser</i> les enfants à faire des expériences (et,
oui, à casser des choses), et à trouver des solutions à leurs problèmes. Les
parents tout autant que les enfants ont besoin de petits coups de pouce pour
apprendre à utiliser le clavier, l'interface, le shell ou les langages de
programmations dans de bonnes conditions.
</p>

<h4>Interface utilisateur</h4>

<p>
Découvrir et implémenter à la fois des interfaces graphiques et des interfaces
en mode texte, utilisables et pratiques pour les enfants. L'idée n'est pas de
réinventer ce qui fonctionne déjà, mais d'améliorer l'existant (gestionnaires
de fenêtres, systèmes de menu, etc.) en fournissant des systèmes préconfigurés
de façon optimale pour les enfants.
</p>

<h4>Formation familiale</h4>

<p> 
Donner aux parents (et dans certains cas, aux aînés) les outils pour apprendre
l'informatique à leurs enfants (ou cadets) et à les guider d'un usage
raisonnablement délimité de l'ordinateur vers un usage de plus en plus
indépendant au fur et à mesure de l'apprentissage. Par exemple, de nombreux
parents souhaitent régler l'usage d'Internet pour protéger leurs enfants
jusqu'à ce qu'ils aient atteint l'âge nécessaire pour aborder des contenus
adultes. Les parents sont les seuls à même de choisir ce qui convient pour
leurs enfants. Debian Junior ne prend pas de décision de ce type, mais offre
les outils et la documentation permettant de faire ces choix. Ceci dit,
je pense que cet objectif doit mettre l'accent plus sur l'accompagnement (coté
positif de la chose) que sur les restrictions (coté plus négatif).
</p>

<h4>Un système pour les enfants</h4>

<p>
Bien que notre premier objectif, en tant qu'administrateur système pour
enfants, est probablement d'ouvrir des comptes pour nos enfants sur nos propres
systèmes, et de les peupler d'applications qui leur plaisent, arrive un moment
où il faut envisager de leur donner leur propre système. La réalisation la plus
ambitieuse de cet objectif pourrait être un équivalent Debian des
ordinateurs-jouets actuellement sur le marché : un système haut en couleurs
et couvert de décalcomanies avec des logiciels préinstallés spécialement conçus
pour les enfants d'une tranche d'âge spécifique. Il est important de garder à
l'esprit qu'il s'agirait encore d'un système Debian, et non d'un nouveau projet
né d'une quelconque scission. C'est parfaitement possible, grâce au système de
gestion des paquets Debian, (par exemple grâce aux métapaquets) et cela ne
nécessiterait pas le développement, séparé de Debian, d'une édition spéciale
pour les enfants.
</p>

<h4>Internationalisation</h4>

<p>
Bien que l'anglais puisse être considéré comme une langue « universelle », ce
n'est pas la langue maternelle de tous les enfants. L'internationalisation
devrait être un but pour Debian dans sa globalité, et ces problèmes sont encore
amplifiés lorsqu'on souhaite s'adresser à des enfants. Ils ne voudront pas
utiliser Debian sans prise en charge de leur langue et préféreront utiliser
d'autres systèmes d'exploitation mieux internationalisés. C'est pourquoi nous
devons garder cela à l'esprit.
</p>
