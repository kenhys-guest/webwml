#use wml::debian::translation-check translation="2a3fa2ac1b49f491737f3c89828986a43c00a61e" maintainer="galaxico"
<define-tag pagetitle>Updated Debian 10: 10.9 released</define-tag>
<define-tag release_date>2021-03-27</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the ninth update of its
stable distribution Debian <release> (codename <q><codename></q>). 
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Miscellaneous Bugfixes</h2>

<p>This stable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction avahi "Remove avahi-daemon-check-dns mechanism, which is no longer needed">
<correction base-files "Update /etc/debian_version for the 10.9 point release">
<correction cloud-init "Avoid logging generated passwords to world-readable log files [CVE-2021-3429]">
<correction debian-archive-keyring "Add bullseye keys; retire jessie keys">
<correction debian-installer "Use 4.19.0-16 Linux kernel ABI">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction exim4 "Fix use of concurrent TLS connections under GnuTLS; fix TLS certificate verification with CNAMEs; README.Debian: document the limitation/extent of server certificate verification in the default configuration">
<correction fetchmail "No longer report <q>System error during SSL_connect(): Success</q>; remove OpenSSL version check">
<correction fwupd "Add SBAT support">
<correction fwupd-amd64-signed "Add SBAT support">
<correction fwupd-arm64-signed "Add SBAT support">
<correction fwupd-armhf-signed "Add SBAT support">
<correction fwupd-i386-signed "Add SBAT support">
<correction fwupdate "Add SBAT support">
<correction fwupdate-amd64-signed "Add SBAT support">
<correction fwupdate-arm64-signed "Add SBAT support">
<correction fwupdate-armhf-signed "Add SBAT support">
<correction fwupdate-i386-signed "Add SBAT support">
<correction gdnsd "Fix stack overflow with overly-large IPv6 addresses [CVE-2019-13952]">
<correction groff "Rebuild against ghostscript 9.27">
<correction hwloc-contrib "Enable support for the ppc64el architecture">
<correction intel-microcode "Update various microcode">
<correction iputils "Fix ping rounding errors; fix tracepath target corruption">
<correction jquery "Fix untrusted code execution vulnerabilities [CVE-2020-11022 CVE-2020-11023]">
<correction libbsd "Fix out-of-bounds read issue [CVE-2019-20367]">
<correction libpano13 "Fix format string vulnerability">
<correction libreoffice "Do not load encodings.py from current directoy">
<correction linux "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction linux-latest "Update to -15 kernel ABI; update for -16 kernel ABI">
<correction linux-signed-amd64 "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction linux-signed-arm64 "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction linux-signed-i386 "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction lirc "Normalize embedded ${DEB_HOST_MULTIARCH} value in /etc/lirc/lirc_options.conf to find unmodified configuration files on all architectures; recommend gir1.2-vte-2.91 instead of non-existent gir1.2-vte">
<correction m2crypto "Fix test failure with recent OpenSSL versions">
<correction openafs "Fix outgoing connections after unix epoch time 0x60000000 (14 January 2021)">
<correction portaudio19 "Handle EPIPE from alsa_snd_pcm_poll_descriptors, fixing crash">
<correction postgresql-11 "New upstream stable release; fix information leakage in constraint-violation error messages [CVE-2021-3393]; fix CREATE INDEX CONCURRENTLY to wait for concurrent prepared transactions">
<correction privoxy "Security issues [CVE-2020-35502 CVE-2021-20209 CVE-2021-20210 CVE-2021-20211 CVE-2021-20212 CVE-2021-20213 CVE-2021-20214 CVE-2021-20215 CVE-2021-20216 CVE-2021-20217 CVE-2021-20272 CVE-2021-20273 CVE-2021-20275 CVE-2021-20276]">
<correction python3.7 "Fix CRLF injection in http.client [CVE-2020-26116]; fix buffer overflow in PyCArg_repr in _ctypes/callproc.c [CVE-2021-3177]">
<correction redis "Fix a series of integer overflow issues on 32-bit systems [CVE-2021-21309]">
<correction ruby-mechanize "Fix command injection issue [CVE-2021-21289]">
<correction systemd "core: make sure to restore the control command id, too, fixing a segfault; seccomp: allow turning off of seccomp filtering via an environment variable">
<correction uim "libuim-data: Perform symlink_to_dir conversion of /usr/share/doc/libuim-data in the resurrected package for clean upgrades from stretch">
<correction xcftools "Fix integer overflow vulnerability [CVE-2019-5086 CVE-2019-5087]">
<correction xterm "Correct upper-limit for selection buffer, accounting for combining characters [CVE-2021-27135]">
</table>


<h2>Security Updates</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2021 4826 nodejs>
<dsa 2021 4844 dnsmasq>
<dsa 2021 4845 openldap>
<dsa 2021 4846 chromium>
<dsa 2021 4847 connman>
<dsa 2021 4849 firejail>
<dsa 2021 4850 libzstd>
<dsa 2021 4851 subversion>
<dsa 2021 4853 spip>
<dsa 2021 4854 webkit2gtk>
<dsa 2021 4855 openssl>
<dsa 2021 4856 php7.3>
<dsa 2021 4857 bind9>
<dsa 2021 4858 chromium>
<dsa 2021 4859 libzstd>
<dsa 2021 4860 openldap>
<dsa 2021 4861 screen>
<dsa 2021 4862 firefox-esr>
<dsa 2021 4863 nodejs>
<dsa 2021 4864 python-aiohttp>
<dsa 2021 4865 docker.io>
<dsa 2021 4867 grub-efi-amd64-signed>
<dsa 2021 4867 grub-efi-arm64-signed>
<dsa 2021 4867 grub-efi-ia32-signed>
<dsa 2021 4867 grub2>
<dsa 2021 4868 flatpak>
<dsa 2021 4869 tiff>
<dsa 2021 4870 pygments>
<dsa 2021 4871 tor>
<dsa 2021 4872 shibboleth-sp>
</table>



<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>


