msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2021-01-08 12:43+0100\n"
"Last-Translator: Szabolcs Siebenhofer\n"
"Language-Team: unknown\n"
"Language: Hungarian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Kulcsszó"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Jelenítsd meg"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "az útvonal a kulcsszóval végződik"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "azokat csomagokat, amelyek ilyen nevű fájlt tartalmaznak"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr ""
"azokat a csomagokat, amelyek olyan fájlokat tartalmaznak, amelyek nevében "
"benne van a kulcsszó"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Disztribúció"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr "kísérleti"

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "unstable"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "testing"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "stable"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "oldstable"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Architektúra"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr "mindegyik"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "Keresés"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "Visszaállítás"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Keresés helye"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Csak csomagnevekben"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "Leírásokban"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "Forráscsomagnevekben"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "Csak a pontos egyezéseket mutasd"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Szekció"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "64-bites PC (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr "64-bites PC (AArch64)"

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr "EABI ARM (armel)"

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr "Hard Float ABI ARM (armhf)"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd 32-bit PC (i386)"

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr "32-bites PC (i386)"

#: ../../english/releases/arches.data:17
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD 32-bit PC (i386)"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD 64-bit PC (amd64)"

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr "MIPS (big endian)"

#: ../../english/releases/arches.data:22
msgid "64-bit MIPS (little endian)"
msgstr "64-bit MIPS (little endian)"

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr "POWER Processzorok"

#: ../../english/releases/arches.data:26
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr "RISC-V 64-bit little endian (riscv64)"

#: ../../english/releases/arches.data:27
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:28
msgid "IBM System z"
msgstr "IBM System z"

#: ../../english/releases/arches.data:29
msgid "SPARC"
msgstr "SPARC"

#~ msgid "packages that contain files or directories named like this"
#~ msgstr ""
#~ "azokat csomagokat, amelyek ilyen nevű fájlt vagy könyvtárat tartalmaznak"

#~ msgid "all files in this package"
#~ msgstr "az összes fájlt ebben a csomagban"

#~ msgid "Case sensitive"
#~ msgstr "Kis- és nagybetűk különböznek"

#~ msgid "no"
#~ msgstr "nem"

#~ msgid "yes"
#~ msgstr "igen"

#~ msgid "MIPS"
#~ msgstr "MIPS"

#~ msgid "MIPS (DEC)"
#~ msgstr "MIPS (DEC)"

#~ msgid "Allow searching on subwords"
#~ msgstr "Részszavakat is keressen"

#~ msgid "Search case sensitive"
#~ msgstr "Kis- és nagybetűk különböznek"

#~ msgid "non-US"
#~ msgstr "non-US"

#~ msgid "Hurd (i386)"
#~ msgstr "Hurd (i386)"

#~ msgid "kFreeBSD (Intel x86)"
#~ msgstr "kFreeBSD (Intel x86)"

#~ msgid "kFreeBSD (AMD64)"
#~ msgstr "kFreeBSD (AMD64)"

#~ msgid "Intel IA-64"
#~ msgstr "Intel IA-64"

#~ msgid "HP PA/RISC"
#~ msgstr "HP PA/RISC"

#~ msgid "AMD64"
#~ msgstr "AMD64"

#~ msgid "Intel x86"
#~ msgstr "Intel x86"
